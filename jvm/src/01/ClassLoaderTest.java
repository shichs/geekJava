import java.io.*;
import java.lang.reflect.InvocationTargetException;

public class ClassLoaderTest extends ClassLoader{
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        Object hello = new ClassLoaderTest().findClass("").newInstance();
        hello.getClass().getMethod("hello").invoke(hello);
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] bytes = null;
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        try {
            FileInputStream inputStream = new FileInputStream("jvm/src/Hello.xlass");
            int read;
            while ((read = inputStream.read()) != -1) {
                bs.write(255 - read);
            }
            bytes = bs.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bytes != null;
        return defineClass(name,bytes,0,bytes.length);
    }
}
