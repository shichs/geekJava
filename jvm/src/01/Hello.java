public class Hello {

    private static final int mb = 1024*1024;

    public static void main(String[] args) {
        int a = 10;
        int b = 11;
        Hello hello = new Hello();
        int c = hello.getMaxNum(a, b);
        for (int i = 0; i < c; i++) {
            a += i;
        }
    }

    public int getMaxNum(int a, int b) {
        return a > b ? a : b;
    }
}
/*
Classfile /D:/developProject/geekJava/jvm/src/Hello.class
  Last modified 2021-9-19; size 690 bytes
  MD5 checksum 4d76091f37ef43ceff04ef7d3feb7e42
  Compiled from "Hello.java"
public class Hello
  minor version: 0
  major version: 52
  flags: ACC_PUBLIC, ACC_SUPER
Constant pool:
   #1 = Methodref          #5.#30         // java/lang/Object."<init>":()V
   #2 = Class              #31            // Hello
   #3 = Methodref          #2.#30         // Hello."<init>":()V
   #4 = Methodref          #2.#32         // Hello.getMaxNum:(II)I
   #5 = Class              #33            // java/lang/Object
   #6 = Utf8               <init>
   #7 = Utf8               ()V
   #8 = Utf8               Code
   #9 = Utf8               LineNumberTable
  #10 = Utf8               LocalVariableTable
  #11 = Utf8               this
  #12 = Utf8               LHello;
  #13 = Utf8               main
  #14 = Utf8               ([Ljava/lang/String;)V
  #15 = Utf8               i
  #16 = Utf8               I
  #17 = Utf8               args
  #18 = Utf8               [Ljava/lang/String;
  #19 = Utf8               a
  #20 = Utf8               b
  #21 = Utf8               hello
  #22 = Utf8               c
  #23 = Utf8               StackMapTable
  #24 = Class              #18            // "[Ljava/lang/String;"
  #25 = Class              #31            // Hello
  #26 = Utf8               getMaxNum
  #27 = Utf8               (II)I
  #28 = Utf8               SourceFile
  #29 = Utf8               Hello.java
  #30 = NameAndType        #6:#7          // "<init>":()V
  #31 = Utf8               Hello
  #32 = NameAndType        #26:#27        // getMaxNum:(II)I
  #33 = Utf8               java/lang/Object
{
  public Hello();
    descriptor: ()V
    flags: ACC_PUBLIC
    Code:
      stack=1, locals=1, args_size=1
         0: aload_0
         1: invokespecial #1                  // Method java/lang/Object."<init>":()V
         4: return
      LineNumberTable:
        line 1: 0
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0       5     0  this   LHello;

  public static void main(java.lang.String[]);
    descriptor: ([Ljava/lang/String;)V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=3, locals=6, args_size=1
         0: bipush        10    //入栈
         2: istore_1            //出栈并保存到局部变量表中槽位1
         3: bipush        11
         5: istore_2
         6: new           #2                  // class Hello   创建对象
         9: dup
        10: invokespecial #3                  // Method "<init>":()V
        13: astore_3            //出栈并保存到局部变量表中槽位3
        14: aload_3
        15: iload_1
        16: iload_2
        17: invokevirtual #4                  // Method getMaxNum:(II)I  执行方法
        20: istore        4     //定义变量4  = getMaxNum()
        22: iconst_0            //常量0
        23: istore        5
        25: iload         5     //加载变量5
        27: iload         4     //加载变量4
        29: if_icmpge     43    //5>4  跳转到43
        32: iload_1             //加载变量a
        33: iload         5     //加载i
        35: iadd                //i自加
        36: istore_1            //变量a放入局部变量表
        37: iinc          5, 1  //（5代表局部变量表中槽位5的变量，1代表局部变量表中槽位1的变量）a+i
        40: goto          25    //跳转到25行
        43: return
      LineNumberTable:
        line 3: 0
        line 4: 3
        line 5: 6
        line 6: 14
        line 7: 22
        line 8: 32
        line 7: 37
        line 10: 43
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
           25      18     5     i   I
            0      44     0  args   [Ljava/lang/String;
            3      41     1     a   I
            6      38     2     b   I
           14      30     3 hello   LHello;
           22      22     4     c   I
      StackMapTable: number_of_entries = 2
        frame_type = 255 / full_frame /
          offset_delta = 25
                  locals = [ class "[Ljava/lang/String;", int, int, class Hello, int, int ]
        stack = []
        frame_type = 250 / chop /
        offset_delta = 17

public int getMaxNum(int, int);
        descriptor: (II)I
        flags: ACC_PUBLIC
        Code:
        stack=2, locals=3, args_size=3
        0: iload_1       //加载a
        1: iload_2       //加载b
        2: if_icmple     9   //如果a>b  跳转到9行
        5: iload_1       //加载a
        6: goto          10  //跳转到10行
        9: iload_2       //加载b
        10: ireturn      返回
        LineNumberTable:
        line 13: 0
        LocalVariableTable:
        Start  Length  Slot  Name   Signature
        0      11     0  this   LHello;
        0      11     1     a   I
        0      11     2     b   I
        StackMapTable: number_of_entries = 2
        frame_type = 9  //same
        frame_type = 64  //same_locals_1_stack_item
        stack = [ int ]
        }
        SourceFile: "Hello.java"
 */