import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;


public class SocketService02 {
    private static Random random = new Random();
    private static AtomicInteger atomicInteger = new AtomicInteger();

    public static void main(String[] args) throws IOException {
        final int i = Runtime.getRuntime().availableProcessors();
        System.out.println(i);
        final ExecutorService executorService = Executors.newFixedThreadPool(i+2);
        final ServerSocket serverSocket = new ServerSocket(8088);
        while (true) {
            final Socket accept = serverSocket.accept();
            executorService.execute(() -> {
                try {
                    exec(accept);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private static void exec(Socket socket) throws IOException {
        PrintWriter printStream = new PrintWriter(socket.getOutputStream(),true);
        printStream.println("HTTP/1.1 200 OK");
        printStream.println("Content-Type:test/html;charset=utf-8");
        String body = "test socket";
        printStream.println("Content-Length:" + body.getBytes().length);
        printStream.println();
        printStream.write(body);
        printStream.close();
        socket.close();
        LongAdder counter = new LongAdder();
//        System.out.println(atomicInteger.getAndIncrement());
        Object garbage = GCLogAnalysis.generateGarbage(100 * 1024);
        counter.increment();
        int cacheSize = 2000;
        Object[] cachedGarbage = new Object[cacheSize];
        int randomIndex = random.nextInt(2 * cacheSize);
        if (randomIndex < cacheSize) {
            cachedGarbage[randomIndex] = garbage;
        }
    }
}
