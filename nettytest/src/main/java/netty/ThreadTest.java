package netty;

public class ThreadTest {
    public static void main(String[] args) {
        final Runnable runnable = () -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final Thread thread = Thread.currentThread();
            System.out.println("当前线程:" + thread.getName());
        };
        System.out.println("12223333444");
        final Thread thread = new Thread(runnable);
        thread.setName("test-thread-1");
        thread.setDaemon(true);
        thread.start();
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
