import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.*;

public class HttpRequestTest {
    public static void main(String[] args) throws IOException {
        final CloseableHttpClient client = HttpClientBuilder.create().build();
        final CloseableHttpResponse response = client.execute(new HttpGet("http://localhost:8081"));
        for (Header allHeader : response.getAllHeaders()) {
            System.out.println("Header name:"+allHeader.getName()+" value:"+allHeader.getValue());
        }
        final HttpEntity entity = response.getEntity();
        System.out.println("contentType : "+entity.getContentType());
        System.out.println("contentLength" + entity.getContentLength());
        System.out.println("content byte :" + entity.getContent().toString());
        final InputStreamReader stream = new InputStreamReader(entity.getContent());
        final BufferedReader reader = new BufferedReader(stream);
        final StringBuilder builder = new StringBuilder();
        String i;
        while ((i = reader.readLine()) != null) {
            builder.append(i);
        }
        client.close();
        reader.close();
        stream.close();
        System.out.println(builder.toString());
    }
}
